<?php

/**
 * @var $model \common\models\Posts
 */
$homeUrl = Yii::$app->homeUrl;
?>

<div class="col-xs-12">
    <div class="row">
        <?php foreach ($model as $post) {

            $pathPostDetails =
                \yii\helpers\Url::to(
                    [
                        'posts/details',
                        'slug' => $post['slug'],
                    ]
                );
            ?>
            <div class="col-xs-4" style="padding: 50px">

                <div class="col-xs-12 title text-center">
                    <h3>
                        <a href=" <?= $pathPostDetails ?>">
                            <?= $post['title'] ?>
                            <span>( <?= $post['user']['username'] ?> )</span>
                        </a>
                    </h3>
                </div>

                <div class="col-xs-12 image">
                    <a href=" <?= $pathPostDetails ?>">

                        <img src="<?= $homeUrl ?>images/posts/small/<?= $post['img_name'] ?>"
                             alt="  <?= $post['title'] ?>">
                    </a>
                </div>

                <div class="col-xs-12 shor-description text-center">
                    <a href=" <?= $pathPostDetails ?>">

                        <?= $post['short_description'] ?>
                    </a>
                </div>

            </div>

        <?php } ?>
    </div>
</div>
