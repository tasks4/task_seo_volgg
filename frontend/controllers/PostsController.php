<?php

namespace frontend\controllers;

use common\models\Posts;
use yii\helpers\Html;
use yii\web\Controller;

class PostsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = Posts::find()->where(['is_status' => true])->orderBy(['created_at' => SORT_DESC])->all();

        return $this->render('index', compact('model'));
    }

    public function actionDetails($slug)
    {
        $model =
            Posts::find()
                ->where(['is_status' => true, 'slug' => Html::encode($slug)])
                ->orderBy(['created_at' => SORT_DESC])
                ->one()
        ;

        return $this->render('details', compact('model'));

    }
}
