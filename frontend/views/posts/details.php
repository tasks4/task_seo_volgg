<?php

/**
 * @var  $model \common\models\Posts
 */

$homeUrl = Yii::$app->homeUrl;
?>

<div class="col-xs-12 text-center">
    <img src="<?= $homeUrl ?>images/posts/<?= $model['img_name'] ?>"
         alt=" <?= $model['title'] ?>"/>
</div>

<div class="col-xs-12 title text-center">
    <h1>
        <?= $model['title'] ?>
        <span>( <?= $model['user']['username'] ?> )</span>
    </h1>
</div>

<br>
<br>
<br>
<br>
<br>
<div class="col-xs-12 shor-description text-center">
    <?= $model['description'] ?>
</div>
