<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostsControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Posts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    if (empty($dataProvider->count)) {
        return Yii::$app->session->setFlash('info', "Your post is an empty");
    }

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'img_name',
                'format' => 'html',
                'value' => function ($data) {
                    return
                        Html::img('/images/posts/small/' . $data['img_name'],
                            ['width' => '100px']);
                },
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($data) {
                    return $data['user']['username'];
                },
            ],

            'slug',
            'title',
            'created_at:datetime',
            'updated_at:datetime',
            //'is_status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    'update' => function ($url, $model, $key) {
                        $options =
                            [
                                'title' => 'Запис можно отредактировать в течение часа после публикации.',
                                'class' => 'disabled',
                                'onclick' => 'return  false',
                            ];

                        if ($model['created_at'] + 3600 > time()) {
                            $options = [];
                        }

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
